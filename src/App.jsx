import React from 'react';
import AppRoutes from './routes/AppRoutes';
import ProjectsProvider from './context/ProjectsContext';

export default function App() {
  return (
    <ProjectsProvider>
      <AppRoutes />
    </ProjectsProvider>
  );
}
