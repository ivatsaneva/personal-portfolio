import axios from 'axios';

const axiosInstance = axios.create({
  baseURL: `https://graphql.contentful.com/content/v1/spaces/${
    import.meta.env.VITE_CONTENTFUL_SPACE_ID
  }/`,
  headers: {
    Authorization: `Bearer ${import.meta.env.VITE_CONTENTFUL_ACCESS_TOKEN}`,
    'Content-Type': 'application/json',
  },
});

export default axiosInstance;
