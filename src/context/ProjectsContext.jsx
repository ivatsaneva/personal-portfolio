import React, { createContext, useEffect, useState } from 'react';
import axiosInstance from '../config/axiosConfig';

export const ProjectsContext = createContext();

const ProjectsProvider = ({ children }) => {
  const [projects, setProjects] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    const fetchProjects = async () => {
      try {
        const response = await axiosInstance.post('', {
          query: `
            query {
              projectCollection {
                items {
                  sys {
                    id
                  }
                  title
                  featureImage {
                    url
                  }
                  introText
                  contribution
                  link
                  secondTitle
                  challenge
                  goal
                  solution
                  projectImage {
                    url
                  }
                  sortOrder
                }
              }
            }
          `,
        });

        const sortedProjects = response.data.data.projectCollection.items.sort(
          (a, b) => {
            const aOrder = a.sortOrder !== undefined ? a.sortOrder : 0;
            const bOrder = b.sortOrder !== undefined ? b.sortOrder : 0;
            return bOrder - aOrder;
          }
        );
        setProjects(sortedProjects);
      } catch (error) {
        console.error(
          'Error fetching projects:',
          error.response ? error.response.data : error.message
        );
        setError(error);
      } finally {
        setLoading(false);
      }
    };

    fetchProjects();
  }, []);

  const fetchProjectById = async (id) => {
    try {
      const response = await axiosInstance.post('', {
        query: `
          query {
            project(id: "${id}") {
              sys {
                id
              }
              title
              featureImage {
                url
              }
              introText
              contribution
              link
              secondTitle
              challenge
              goal
              solution
              projectImage {
                url
              }
              sortOrder
            }
          }
        `,
      });
      return response.data.data.project;
    } catch (error) {
      console.error(
        'Error fetching project:',
        error.response ? error.response.data : error.message
      );
      throw error;
    }
  };

  return (
    <ProjectsContext.Provider
      value={{ projects, fetchProjectById, loading, error }}
    >
      {children}
    </ProjectsContext.Provider>
  );
};

export default ProjectsProvider;
