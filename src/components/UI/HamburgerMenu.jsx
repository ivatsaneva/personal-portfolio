import React from "react";
import { useState } from "react";
import classNames from 'classnames';

function HamburgerMenu({ opened }) {
    return (
      <div
        className={classNames('tham tham-e-squeeze tham-w-6', {
          'tham-active': opened,
        })}
      >
        <div className="tham-box">
          <div className="tham-inner" />
        </div>
      </div>
    );
  }

export default HamburgerMenu;
