import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Logo from "../../assets/images/logo.svg";
import HamburgerMenu from "./HamburgerMenu";

const Header = () => {
  const [menuOpen, setMenuOpen] = useState(false);

  const toggleMenu = () => {
    setMenuOpen(!menuOpen);
  };

  useEffect(() => {
    const handleResize = () => {
      if (window.innerWidth >= 640) {
        setMenuOpen(false);
      }
    };

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return (
    <header className="container mx-auto px-5 py-6 md:px-10 lg:px-20">
      <div className="flex items-center justify-between">
        <Link to="/">
          <img src={Logo} alt="Iva Tsaneva Logo" className="block max-w-12" />
        </Link>
        <div className="hidden items-center justify-between sm:flex">
          <nav className="font-clash font-medium uppercase">
            <ul className="flex space-x-6">
              <li>
                <Link to="/" className="hover:underline">
                  Home
                </Link>
              </li>
              <li>
                <Link to="/work" className="hover:underline">
                  Work
                </Link>
              </li>
            </ul>
          </nav>
          <Link
            to="mailto:ivatsaneva21@gmail.com"
            className="ml-6 rounded-full border border-transparent bg-primary px-5 py-3 font-clash font-medium uppercase text-black transition duration-300 ease-in-out hover:border-black hover:bg-transparent hover:text-black"
          >
            Get in touch
          </Link>
        </div>
        <div className="sm:hidden" onClick={toggleMenu}>
          <HamburgerMenu opened={menuOpen} />
        </div>
      </div>
      {menuOpen && (
        <nav className="justify-left absolute left-0 z-50 flex w-full flex-col bg-white p-8 font-clash font-medium uppercase">
          <ul className="space-y-4">
            <li>
              <Link
                to="/"
                className="hover:underline"
                onClick={() => setMenuOpen(false)}
              >
                Home
              </Link>
            </li>
            <li>
              <Link
                to="/work"
                className="hover:underline"
                onClick={() => setMenuOpen(false)}
              >
                Work
              </Link>
            </li>
            <li>
              <Link
                to="mailto:ivatsaneva21@gmail.com"
                className="inline-block rounded-full border border-transparent bg-primary px-5 py-3 font-clash font-medium uppercase text-black transition duration-300 ease-in-out hover:border-black hover:bg-transparent hover:text-black"
                onClick={() => setMenuOpen(false)}
              >
                Get in touch
              </Link>
            </li>
          </ul>
        </nav>
      )}
    </header>
  );
};

export default Header;
