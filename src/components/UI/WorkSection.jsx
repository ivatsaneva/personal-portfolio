import React, { useContext, useState } from 'react';
import Marquee from 'react-fast-marquee';
import { ProjectsContext } from '../../context/ProjectsContext';
import { Link } from 'react-router-dom';
import FollowPointer from './FollowPointer';

function WorkSection() {
  const { projects, loading, error } = useContext(ProjectsContext);
  const [isCursorVisible, setCursorVisible] = useState(false);

  const featuredProjects = projects.slice(0, 2);

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error: {error.message}</p>;

  const workItem = ' Work · ';
  const repeatCount = 20;

  const workItems = Array.from({ length: repeatCount }, (_, index) => (
    <li key={index} className="flex flex-shrink-0 mx-2">
      {workItem}
    </li>
  ));

  const formatProjectName = (name) => {
    return name.toLowerCase().replace(/ /g, '-');
  };

  const handleMouseEnter = () => {
    setCursorVisible(true);
  };

  const handleMouseLeave = () => {
    setCursorVisible(false);
  };

  return (
    <>
      <FollowPointer isVisible={isCursorVisible} />
      <Marquee
        gradient={false}
        speed={50}
        loop={0}
        direction="left"
        className="font-clash font-medium text-[2rem] xl:text-[4rem] py-3 w-full border-t-2 border-b-0 border-gray-400 overflow-hidden mb-3"
      >
        {workItems}
      </Marquee>

      <div className="container mx-auto px-5 lg:px-20 md:px-10 pb-20 xl:pb-28">
        <div className="pb-14 xl:pb-20 block">
          <ul className="grid grid-cols-1 gap-y-8 gap-x-4 md:gap-y-10 md:gap-x-6 lg:grid-cols-2 lg:gap-y-12 lg:gap-x-8">
            {featuredProjects.map((work) => (
              <li key={work.sys.id} className="relative">
                <Link
                  to={`/work/${formatProjectName(work.title)}`}
                  className="block border-solid border-[6px] border-transparent hover:border-primary transition-all duration-400 ease-in-out mb-3"
                  onMouseEnter={handleMouseEnter}
                  onMouseLeave={handleMouseLeave}
                >
                  <div className="relative w-full pb-[100%] overflow-hidden">
                    {work.projectImage && (
                      <img
                        src={work.featureImage.url}
                        alt={work.title}
                        className="absolute inset-0 w-full h-full object-cover"
                        style={{ cursor: 'none' }}
                      />
                    )}
                  </div>
                </Link>
                <div className="project-details p-1">
                  <h3 className="text-[1.5rem] leading-6 pb-1 uppercase tracking-wide">
                    <Link to={`/work/${formatProjectName(work.title)}`}>
                      {work.title}
                    </Link>
                  </h3>
                  <p className="text-[0.98rem] text-zinc-500 font-clash font-medium uppercase tracking-wide">
                    {work.contribution}
                  </p>
                </div>
              </li>
            ))}
          </ul>
        </div>
        <div className="flex justify-center">
          <Link
            to={'/work'}
            className="px-5 py-3 bg-white font-clash text-black font-medium rounded-full uppercase border border-black transition duration-300 ease-in-out hover:bg-primary hover:text-black hover:border-primary"
          >
            More Work
          </Link>
        </div>
      </div>
    </>
  );
}

export default WorkSection;
