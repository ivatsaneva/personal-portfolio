import React, { useEffect, useState } from 'react';
import { motion, useMotionValue, useSpring } from 'framer-motion';

const FollowPointer = ({ isVisible }) => {
  const x = useMotionValue(-100);
  const y = useMotionValue(-100);

  const springX = useSpring(x, { stiffness: 270, damping: 29 });
  const springY = useSpring(y, { stiffness: 270, damping: 29 });

  useEffect(() => {
    const moveCursor = (e) => {
      x.set(e.clientX - 56);
      y.set(e.clientY - 56);
    };

    window.addEventListener('mousemove', moveCursor);

    return () => {
      window.removeEventListener('mousemove', moveCursor);
    };
  }, [x, y]);

  return (
    <motion.div
      className={`fixed top-0 left-0 z-50 w-28 h-28 bg-primary text-black flex font-clash font-medium items-center justify-center rounded-full pointer-events-none tracking-wider ${
        isVisible ? 'block' : 'hidden'
      }`}
      style={{ x: springX, y: springY }}
      initial={{ opacity: 0 }}
      animate={{ opacity: isVisible ? 1 : 0 }}
    >
      VIEW
    </motion.div>
  );
};

export default FollowPointer;
