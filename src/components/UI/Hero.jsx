import HeroImage from '../../assets/images/intro-hero-img.jpg';
import LargeTitleSVG from '../../assets/images/hero-large-title.svg';
import Marquee from 'react-fast-marquee';

function Hero() {
  return (
    <div className="container mx-auto px-5 lg:px-20 md:px-10">
      <div className="flex flex-col gap-3 md:gap-5 py-6 lg:py-10">
        <img
          src={LargeTitleSVG}
          alt="Hero Title SVG"
          className="block w-full"
        />
        <h1 className="uppercase font-sans font-medium text-xl sm:text-3xl md:text-4xl xl:text-5xl">
          Crafting digital experiences
        </h1>
      </div>
      <div>
        <div>
          <Marquee
            gradient={false}
            speed={50}
            loop={0}
            direction="left"
            className="bg-primary py-2 uppercase font-clash tracking-widest"
          >
            CMS Mastery · ReactJS · Web Design · Bespoke Web Solutions · Web
            Development · CMS Mastery · ReactJS · Web Design · Bespoke Web
            Solutions · Web Development · CMS Mastery · ReactJS · Web Design ·
            Bespoke Web Solutions · Web Development ·
          </Marquee>
        </div>
        <img src={HeroImage} alt="Hero Title SVG" className="block" />
      </div>
    </div>
  );
}
export default Hero;
