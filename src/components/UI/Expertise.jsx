function Expertise() {
  return (
    <div className="container mx-auto px-5 py-20 md:px-10 lg:px-20 xl:py-28">
      <div className="flex flex-col gap-10 px-0 lg:px-16 xl:gap-16 xl:px-20">
        <div>
          <h2 className="font-semi text-5xl uppercase xl:text-7xl">
            What I do
          </h2>
        </div>
        <div className="flex flex-col gap-6">
          <div className="section-wrapper xl:items-left flex flex-col justify-between gap-12 bg-gray px-6 py-12 xl:flex-row xl:gap-28 xl:px-12 xl:py-16">
            <div className="section-info flex w-full flex-col gap-5 xl:w-[50%]">
              <h2 className="text-2xl font-medium uppercase xl:text-3xl">
                01 . Web Development
              </h2>
              <p>
                Bringing 6 years of experience, I've been transforming ideas
                into dynamic, user-friendly websites. Specializing in CMS
                development and ReactJS, I create seamless and engaging digital
                experiences.
              </p>
            </div>
            <div className="expertise-list w-full xl:w-[50%]">
              <ul className="[&>li]border-gray-300 [&>li]:font-semi flex flex-wrap gap-2 xl:gap-3 [&>li]:rounded-full [&>li]:border [&>li]:px-4 [&>li]:py-2 [&>li]:text-[1rem] [&>li]:xl:text-[1.4rem]">
                <li>ReactJS</li>
                <li>JavaScript</li>
                <li>TypeScript</li>
                <li>RESTful APIs</li>
                <li>Context API</li>
                <li>Wordpress</li>
                <li>Webflow</li>
                <li>Tailwind</li>
                <li>CSS & SASS/SCSS</li>
              </ul>
            </div>
          </div>
          <div className="section-wrapper xl:items-left flex flex-col justify-between gap-12 bg-gray px-6 py-12 xl:flex-row xl:gap-28 xl:px-12 xl:py-16">
            <div className="section-info flex w-full flex-col gap-5 xl:w-[50%]">
              <h2 className="text-2xl font-medium uppercase xl:text-3xl">
                02 . Web Design
              </h2>
              <p>
                Specializing in creating exceptional user experiences and
                intuitive user interfaces, I transform concepts into visually
                stunning and functional designs.
              </p>
            </div>
            <div className="expertise-list w-full xl:w-[50%]">
              <ul className="[&>li]border-gray-300 [&>li]:font-semi flex flex-wrap gap-2 xl:gap-3 [&>li]:rounded-full [&>li]:border [&>li]:px-4 [&>li]:py-2 [&>li]:text-[1rem] [&>li]:xl:text-[1.4rem]">
                <li>User Experience</li>
                <li>User Interface</li>
                <li>Prototypoing</li>
                <li>Wireframing</li>
                <li>Figma</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
export default Expertise;
