import React from 'react';
import Marquee from 'react-fast-marquee';
import { Link } from 'react-router-dom';
import Asterisk from '../../assets/images/asterisk.svg';

function Footer() {
  const workItem = 'Contact ·';
  const repeatCount = 20;

  const workItems = Array.from({ length: repeatCount }, (_, index) => (
    <li key={index} className="flex flex-shrink-0 mr-2">
      {workItem}
    </li>
  ));

  return (
    <>
      <Marquee
        gradient={false}
        speed={50}
        loop={0}
        direction="left"
        className="bg-primary py-2 uppercase font-clash tracking-widest"
      >
        {workItems}
      </Marquee>
      <div>
        <div className="footer-container flex flex-col gap-10 items-center bg-black py-[100px]">
          <img src={Asterisk} alt="Asterisk Icon" />
          <h2 className="font-semi uppercase text-5xl xl:text-7xl text-white text-center">
            Get in touch
            <br />
            with me
          </h2>
          <Link
            to="mailto:ivatsaneva21@gmail.com"
            className="px-5 py-3 bg-primary font-clash text-black font-medium rounded-full uppercase border border-transparent transition duration-300 ease-in-out hover:bg-transparent hover:text-white hover:border-white"
          >
            Just Say Hi.
          </Link>
        </div>
        <div className="bg-black pb-6 text-center">
          <p className=" text-sm text-white uppercase font-clash">
            © 2024 Made by Iva Tsaneva
          </p>
        </div>
      </div>
    </>
  );
}

export default Footer;
