import React, { useContext, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { ProjectsContext } from "../../context/ProjectsContext";
import Header from "../UI/Header";
import Footer from "../UI/Footer";
import { Link } from "react-router-dom";
import Asterisk from "../../assets/images/asterisk.svg";
import Marquee from "react-fast-marquee";

function Project() {
  const { name } = useParams();
  const { projects } = useContext(ProjectsContext);
  const [project, setProject] = useState(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    const getProject = () => {
      try {
        const projectData = projects.find(
          (project) => project.title.toLowerCase().replace(/ /g, "-") === name,
        );
        if (projectData) {
          setProject(projectData);
        } else {
          throw new Error("Project not found");
        }
      } catch (error) {
        setError(error);
      } finally {
        setLoading(false);
      }
    };

    if (projects.length) {
      getProject();
    }
  }, [name, projects]);

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error: {error.message}</p>;
  if (!project) return <p>Project not found</p>;

  const workItem = " Gallery · ";
  const repeatCount = 20;

  const workItems = Array.from({ length: repeatCount }, (_, index) => (
    <li key={index} className="mx-2 flex flex-shrink-0">
      {workItem}
    </li>
  ));

  return (
    <>
      <Header />
      <div className="container mx-auto px-5 pb-20 md:px-10 lg:px-20 xl:pb-28">
        <div className="py-10 text-center xl:py-20">
          <h1 className="font-semi text-5xl uppercase text-black xl:text-7xl">
            {project.title}
          </h1>
        </div>
        <div className="flex flex-wrap justify-between gap-10 px-0 pb-12 md:gap-14 md:px-10 lg:gap-16 lg:px-20 xl:pb-16">
          <div className="w-[100%] xl:w-[40%]">
            <p>{project.introText}</p>
          </div>
          <div className="">
            <span className="block pb-3 text-[0.8rem] font-medium uppercase tracking-widest">
              Contribution
            </span>
            <p className="text-lg font-medium uppercase">
              {project.contribution}
            </p>
          </div>

          <div className="">
            <div>
              <Link
                to={project.link}
                target="_blank"
                rel="nofollow"
                className="inline-block rounded-full border border-black bg-white px-5 py-4 font-clash font-medium uppercase text-black transition duration-300 ease-in-out hover:border-primary hover:bg-primary hover:text-black"
              >
                Link to website
              </Link>
            </div>
          </div>
        </div>
        <div className="image-wrap">
          <img
            src={project.featureImage.url}
            alt={project.title}
            className="project-image w-full"
          />
        </div>
        <div className="gap-12 px-0 pt-16 lg:px-20 lg:pt-24 xl:gap-28 xl:pt-32">
          <div className="flex flex-row flex-wrap justify-between gap-12 md:flex-row xl:gap-24">
            <div className="xl:w-[38%]">
              <img src={Asterisk} alt="Asterisk Icon" className="pb-8" />
              <h2 className="font-semi text-4xl text-black md:text-5xl xl:text-6xl">
                {project.secondTitle}
              </h2>
            </div>
            <div className="flex-1 [&>div]:mb-7">
              <div className="section-wrapper xl:items-left flex flex-col justify-between gap-5 bg-gray px-6 py-12 xl:px-12 xl:py-16">
                <h2 className="text-2xl font-medium uppercase">
                  01 . Challenge
                </h2>
                <p>{project.challenge}</p>
              </div>
              <div className="section-wrapper xl:items-left flex flex-col justify-between gap-5 bg-gray px-6 py-12 xl:px-12 xl:py-16">
                <h2 className="text-2xl font-medium uppercase">02 . Goal</h2>
                <p>{project.goal}</p>
              </div>
              <div className="section-wrapper xl:items-left flex flex-col justify-between gap-5 bg-gray px-6 py-12 xl:px-12 xl:py-16">
                <h2 className="text-2xl font-medium uppercase">
                  03 . Solution
                </h2>
                <p>{project.solution}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Marquee
        gradient={false}
        speed={50}
        loop={0}
        direction="left"
        className="border-gray-400 mb-3 w-full overflow-hidden border-b-0 border-t-2 py-3 font-clash text-[2rem] font-medium xl:text-[4rem]"
      >
        {workItems}
      </Marquee>

      <div className="container mx-auto px-5 pb-20 md:px-10 lg:px-20 xl:pb-28">
        <img
          src={project.projectImage.url}
          alt={project.title}
          className="project-image w-full"
        />
      </div>
      <Footer />
    </>
  );
}

export default Project;
