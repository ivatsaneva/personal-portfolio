import Expertise from '../UI/Expertise';
import Footer from '../UI/Footer';
import Header from '../UI/Header';
import Hero from '../UI/Hero';
import WorkSection from '../UI/WorkSection';

function Home() {
  return (
    <>
      <Header />
      <Hero />
      <Expertise />
      <WorkSection />
      <Footer/>
    </>
  );
}
export default Home;
