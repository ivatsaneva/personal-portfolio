import React, { useContext, useState } from "react";
import Header from "../UI/Header";
import { ProjectsContext } from "../../context/ProjectsContext";
import { Link } from "react-router-dom";
import FollowPointer from "../UI/FollowPointer";
import Footer from "../UI/Footer";

function Work() {
  const { projects, loading, error } = useContext(ProjectsContext);
  const [isCursorVisible, setCursorVisible] = useState(false);

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error: {error.message}</p>;

  const formatProjectName = (name) => {
    return name.toLowerCase().replace(/ /g, "-");
  };

  const handleMouseEnter = () => {
    setCursorVisible(true);
  };

  const handleMouseLeave = () => {
    setCursorVisible(false);
  };

  return (
    <>
      <Header />
      <FollowPointer isVisible={isCursorVisible} />

      <div className="container mx-auto px-5 pb-20 md:px-10 lg:px-20 xl:pb-28">
        <div className="py-10 text-center xl:py-20">
          <h1 className="font-semi text-5xl uppercase text-black xl:text-7xl">
            Work
          </h1>
        </div>
        <ul className="grid grid-cols-1 gap-x-4 gap-y-8 md:gap-x-6 md:gap-y-10 lg:grid-cols-2 lg:gap-x-8 lg:gap-y-12">
          {projects.map((work) => (
            <li key={work.sys.id} className="relative">
              <Link
                to={`/work/${formatProjectName(work.title)}`}
                className="duration-400 mb-3 block border-[6px] border-solid border-transparent transition-all ease-in-out hover:border-primary"
                onMouseEnter={handleMouseEnter}
                onMouseLeave={handleMouseLeave}
              >
                <div className="relative w-full overflow-hidden pb-[100%]">
                  {work.projectImage && (
                    <img
                      src={work.featureImage.url}
                      alt={work.title}
                      className="absolute inset-0 h-full w-full object-cover"
                      style={{ cursor: "none" }}
                    />
                  )}
                </div>
              </Link>
              <div className="project-details p-1">
                <h3 className="pb-1 text-[1.5rem] uppercase leading-6 tracking-wide">
                  <Link to={`/work/${formatProjectName(work.title)}`}>
                    {work.title}
                  </Link>
                </h3>
                <p className="font-clash text-[0.98rem] font-medium uppercase tracking-wide text-zinc-500">
                  {work.contribution}
                </p>
              </div>
            </li>
          ))}
        </ul>
      </div>
      <Footer />
    </>
  );
}
export default Work;
