import React from 'react';
import { Routes, Route, useLocation } from 'react-router-dom';

import Home from '../components/pages/Home';
import Work from '../components/pages/Work';
import Project from '../components/pages/Project';
import ScrollToTop from '../components/UI/ScrollToTop';

const AppRoutes = () => {
  const location = useLocation();

  return (
    <>
      <ScrollToTop />
      <Routes key={location.pathname} location={location}>
        <Route index path="/" element={<Home />} />
        <Route path="/work" element={<Work />} />
        <Route path="/work/:name" element={<Project />} />
      </Routes>
    </>
  );
};

export default AppRoutes;
