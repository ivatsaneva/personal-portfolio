# README

# Personal Portfolio

This is my personal portfolio website, developed using React and Vite. It features my work, skills, and projects, with content managed through Contentful and data fetched via GraphQL API.

## Technologies Used

- **React**: A JavaScript library for building user interfaces.
- **Vite**: A build tool that aims to provide a faster and leaner development experience.
- **Tailwind CSS**: A utility-first CSS framework for rapid UI development.
- **Contentful**: A headless CMS to manage and store content.
- **GraphQL**: A query language for APIs to fetch data.

## Instructions to Run the Project

To run the project locally in a DEV environment, please follow these steps:

1. Clone the GitLab repository: [GitLab Project Link](https://gitlab.com/ivatsaneva/personal-portfolio.git).
2. Install the necessary dependencies by running the following command in your terminal:

   ```
   npm install
   ```

3. Start the development server with the following command:

   ```
   npm run dev
   ```

4. Access the project in your browser by visiting the following URL:

   ```
   https://localhost:3000
   ```

Note: Make sure you have Node.js and npm installed on your machine.

That's it! You should now be able to view and interact with the project.

