/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./index.html', './src/**/*.{js,ts,jsx,tsx}'],
  theme: {
    extend: {
      colors: {
        primary: '#D2F65A',
        gray: '#F8F8F8',
        black: '#000000',
        text: '#000000',
      },
      fontFamily: {
        sans: ['Satoshi', 'sans-serif'],
        clash: ['Clash Display', 'sans-serif'],
      },
      spacing: {
        '5rem': '5rem', // 80px = 5rem
      },
      container: {
        center: true,
        padding: {
          
          '2xl': '8rem',
        },
        screens: {
          '2xl': '1920px',
        },
      },
    },
  },
  plugins: [
    require('tailwind-hamburgers'),
  ],
};
